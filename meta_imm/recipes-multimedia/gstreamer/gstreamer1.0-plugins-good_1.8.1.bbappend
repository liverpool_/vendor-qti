EXTRA_OECONF_append = " --enable-v4l2-probe"

SRC_URI += "https://github.com/hizukiayaka/gst-plugins-good/commit/b918646826b1c6708a9ef2243e13afda4be45004.patch;name=patch01 \
	https://github.com/hizukiayaka/gst-plugins-good/commit/5678f71c2516275df75e104ed360dbddde881586.patch;name=patch02 \
       https://github.com/hizukiayaka/gst-plugins-good/commit/78c381a105b0c4b7b3c6ca40646c23d0a78dba7a.patch;name=patch03 \
       https://github.com/hizukiayaka/gst-plugins-good/commit/2c787a28483e233dafb57956a93eae19d77c3c36.patch;name=patch04 \
       https://github.com/hizukiayaka/gst-plugins-good/commit/7f97a3addab4591560290e660c805076903f33a3.patch;name=patch05 \
       https://github.com/hizukiayaka/gst-plugins-good/commit/6ae8bdd701f8700e129465d1f5da68fb761fb2f2.patch;name=patch06 \
       https://github.com/hizukiayaka/gst-plugins-good/commit/790a651bc1e9afdd481635394202b5283ae0ebc3.patch;name=patch07 \
       https://github.com/hizukiayaka/gst-plugins-good/commit/84836dccc80e40ac39045c01ae1542aacd8dc238.patch;name=patch08 \
       https://github.com/hizukiayaka/gst-plugins-good/commit/a51578852eec1452e2e858096c280fbccfb440ec.patch;name=patch09 \
       https://github.com/hizukiayaka/gst-plugins-good/commit/3f77410b2feab96f08ee0eb8815c0cccab609171.patch;name=patch10 \
       https://github.com/hizukiayaka/gst-plugins-good/commit/8175dcab19627f8da9a6400c48a0ada4ef800a40.patch;name=patch11 \
"
SRC_URI[patch01.md5sum] = "d9fe97b24e44bdc7f7c372c931a38ecd"
SRC_URI[patch01.sha256sum] = "12323495f3a88f137bc589d0b59964fbf8971a8fe2e1e9ed4286499f641ace9e"

SRC_URI[patch02.md5sum] = "ab8192d3be06800ba4f7f5bc4cc59829"
SRC_URI[patch02.sha256sum] = "ec7594ae7e43c484b7831f49c45f958e8f1beded30a5595586d4cd80ac011ed9"

SRC_URI[patch03.md5sum] = "00115565051a3889bb757f347e7380a0"
SRC_URI[patch03.sha256sum] = "364868cfb5a1cd001e40c9af548a13cf719499aa6bd504b7483996d96f68577d"

SRC_URI[patch04.md5sum] = "da40ee7fec438b09aaf2eaa7e9f42e34"
SRC_URI[patch04.sha256sum] = "96fef401b9877751ba0874feccd002bad07cd02698cb78cf154cc55b68aed22d"

SRC_URI[patch05.md5sum] = "683c68ff2b3cb51d6f5aa13c695ae1c2"
SRC_URI[patch05.sha256sum] = "a7bed2ea88fe55224b515da1ff45cbabff443677136a32253860a720c8b3f46d"

SRC_URI[patch06.md5sum] = "eb00651b8f52872e29f52cdb803067cd"
SRC_URI[patch06.sha256sum] = "32dfc5789a36325abfa1f1549aa6f6ae35d0fb73ad426185cd96d3cb561b573d"

SRC_URI[patch07.md5sum] = "abbef61b0b9c4317527b4fc8a2606f94"
SRC_URI[patch07.sha256sum] = "480e52495f88b9a4210655b415a2699b1b6ce5b61adda3d20b604e4d879b6c8e"

SRC_URI[patch08.md5sum] = "20388b7f5fc7d9a84d82d31ce243a844"
SRC_URI[patch08.sha256sum] = "00900a12e02af9f17d27324ebad4607084dec317029cf433e2ee0ac744cea0c7"

SRC_URI[patch09.md5sum] = "a64ef9fd7b18de0a208500fdab6d5c56"
SRC_URI[patch09.sha256sum] = "459ac538bf6a87c750960195bc8d8509a55ef91e3ecfb76ae62f5450c7576462"

SRC_URI[patch10.md5sum] = "ce1f13d63adc896a219a3eb93fb17078"
SRC_URI[patch10.sha256sum] = "bdc75772d9d90cb3993b7039dadb04ce7deb67f327a805c1e54220dc2185f998"

SRC_URI[patch11.md5sum] = "36152afce133ae7cbf873e2a8688af2b"
SRC_URI[patch11.sha256sum] = "1b894dcdb94e5788adbc3ccb816b7ddf24687cf0f1f00142fb465ce5c9e006aa"
