PACKAGECONFIG[wayland]         = "--enable-wayland,--disable-wayland,wayland"
PACKAGECONFIG[egl]             = "--enable-egl,--disable-egl,virtual/egl"

PACKAGECONFIG_append = " egl gles2"
