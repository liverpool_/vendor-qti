PACKAGECONFIG_append = " v4l2"

SRC_URI += "\
https://source.codeaurora.org/quic/imm/imm/gst-plugins-good/patch/?id=76a6b804edc54cf78f4a6250e79108445ff61b82;downloadfilename=patch01.patch;name=patch01 \
https://source.codeaurora.org/quic/imm/imm/gst-plugins-good/patch/?id=b130deff607998cc1b3736d017e9e87e2945f1a5;downloadfilename=patch02.patch;name=patch02 \
https://source.codeaurora.org/quic/imm/imm/gst-plugins-good/patch/?id=ba3c90b58e17367318f08edc5d7ae6fc78a4f1ad;downloadfilename=patch03.patch;name=patch03 \
https://source.codeaurora.org/quic/imm/imm/gst-plugins-good/patch/?id=069dec4eee12c45d443235215799fbf158e71137;downloadfilename=patch04.patch;name=patch04 \
https://source.codeaurora.org/quic/imm/imm/gst-plugins-good/patch/?id=c6fb89ba0366e436334d0371ddad0b252f3d3065;downloadfilename=patch05.patch;name=patch05 \
https://source.codeaurora.org/quic/imm/imm/gst-plugins-good/patch/?id=c1f2bfd2f37bdad368004229424a3acbd6d4992c;downloadfilename=patch06.patch;name=patch06 \
https://source.codeaurora.org/quic/imm/imm/gst-plugins-good/patch/?id=d5261fc95b8dcdbae2ae96f2a43876b474479cca;downloadfilename=patch07.patch;name=patch07 \
https://source.codeaurora.org/quic/imm/imm/gst-plugins-good/patch/?id=45b039632a49219c954a5e6d7f28fa3d85adbd33;downloadfilename=patch08.patch;name=patch08 \
https://source.codeaurora.org/quic/imm/imm/gst-plugins-good/patch/?id=89957315847c027f6adab179e6929af22b6f2c54;downloadfilename=patch09.patch;name=patch09 \
https://source.codeaurora.org/quic/imm/imm/gst-plugins-good/patch/?id=1a4b231fdde7e744f9d918769d516f21e2498959;downloadfilename=patch10.patch;name=patch10 \
https://source.codeaurora.org/quic/imm/imm/gst-plugins-good/patch/?id=848cb3a988412ac2b56063fe65654224ef17b022;downloadfilename=patch11.patch;name=patch11 \
https://source.codeaurora.org/quic/imm/imm/gst-plugins-good/patch/?id=3825320c95740d57501638563cdf2546dc016846;downloadfilename=patch12.patch;name=patch12 \
"

SRC_URI[patch01.md5sum] = "0eedfe4e19dd1ecb57d494bba99a11a2"
SRC_URI[patch01.sha256sum] = "b999d5fd0f942e53393a09eb1924d4490e562512de84e5b30d4a788e4ede6fa7"

SRC_URI[patch02.md5sum] = "d2b26c16c448c56c402f58880da36d35"
SRC_URI[patch02.sha256sum] = "97cb0a0a1cd49692cda5b9717b989e992db17fcf034ac1d5e279a82d34380d07"

SRC_URI[patch03.md5sum] = "61382cff81ae36fa658c46be1c1440be"
SRC_URI[patch03.sha256sum] = "b6533f3fc067654ff1218afc9f9973dfeb41706e008ba3e06e70e06fcfd38c14"

SRC_URI[patch04.md5sum] = "d410381fffbbc83c504baf2fc6553ac1"
SRC_URI[patch04.sha256sum] = "0f7de13d452182ef18218b6d76540265d257659cea4b515a494c19c6f1ecc26a"

SRC_URI[patch05.md5sum] = "75c253142672e21684439291c15621fa"
SRC_URI[patch05.sha256sum] = "17ea0ca23b4f050abb91d94e4f66ccf93b973fac9e0707daeaa0deaa042f56d6"

SRC_URI[patch06.md5sum] = "4cb0ac04acb4a89748e4a6e20fcbd4a3"
SRC_URI[patch06.sha256sum] = "8619a0b733c859f40062e2df06bf84934ef18995a0c190c5da5b259f8e364717"

SRC_URI[patch07.md5sum] = "3df00a70300eca7470414d8d9f1b7191"
SRC_URI[patch07.sha256sum] = "73e74f19cb8b095a43d3e166c04398229bca74e9239cee894c6f990480aa47dd"

SRC_URI[patch08.md5sum] = "679e66e598648ddd6d0f5ecea9103e98"
SRC_URI[patch08.sha256sum] = "d000f154501f4805e87eb994357ec54dcd4a1ba302a427ff166f93dfe18f14cc"

SRC_URI[patch09.md5sum] = "5e21f43273afd3ca1d0058458e0cdf2f"
SRC_URI[patch09.sha256sum] = "193336242c8bcf6c70c498822404deb4857fab8e4be965354e59577f123dcc24"

SRC_URI[patch10.md5sum] = "a0ad7ed17efbccecadb8c758fd399309"
SRC_URI[patch10.sha256sum] = "5eab2123d16af2c2b20a8255800c28181a28481dfaf5f965d798d1cf19cf3d28"

SRC_URI[patch11.md5sum] = "a10e515c3d8ee4546850a00a438039f5"
SRC_URI[patch11.sha256sum] = "e607afc761c73ca7b1b3fd72b652e552ee2d6e160874b90efe84c4b92551f88f"

SRC_URI[patch12.md5sum] = "12bb347d2a1808f0f0df2f90f3958036"
SRC_URI[patch12.sha256sum] = "9d275c0c7266bd39b58098b5817a9a278fb1fa7529c0a9e4d8d251b94faa10c9"
