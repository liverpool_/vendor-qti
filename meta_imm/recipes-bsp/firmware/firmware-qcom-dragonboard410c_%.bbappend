FILESPATH =+ "${TOPDIR}/../sources/vendor/qti/firmware-qcom-dragonboard410c:"
SRC_URI += "\
file://firmware-partition-update.service"

inherit systemd

do_install_append() {
    install -d ${D}${systemd_unitdir}/system
    install -m 0644 ${WORKDIR}/firmware-partition-update.service ${D}${systemd_unitdir}/system
}

SYSTEMD_SERVICE_${PN} = "firmware-partition-update.service"
SYSTEMD_AUTO_ENABLE_${PN} = "disable"
