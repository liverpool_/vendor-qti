inherit externalsrc

OVERRIDES_append = ":${BUILD_FLAVOR}"
EXTERNALSRC_local = "${TOPDIR}/../sources/kernel"
LINUX_LINARO_QCOM_GIT_user = "${USER_SRC_URI_KERNEL}"
LINUX_LINARO_QCOM_GIT_caf = "git://${TOPDIR}/../downloads/git2/source.codeaurora.org.quic.imm.imm.kernel.git;protocol=file"
SRCBRANCH_local = "IMM.LE.1.0"
SRCBRANCH_user = "${USER_SRCBRANCH_KERNEL}"
SRCBRANCH_caf = "IMM.LE.1.0"
SRCREV_user = "${USER_SRCREV_KERNEL}"
SRCREV = "2d70f82a97e107b934e9977fad0ca2e6fc6a2443"
